package ee.valeri.homework1;

import java.util.List;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;
import static java.time.Instant.now;

class QueueBuilder implements Runnable {

    private final List<String> queue;

    QueueBuilder(List<String> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (!currentThread().isInterrupted()) {
            queue.add(now().toString());
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                currentThread().interrupt();
            }
        }
    }
}
