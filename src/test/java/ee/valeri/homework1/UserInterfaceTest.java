package ee.valeri.homework1;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import static java.lang.System.lineSeparator;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
class UserInterfaceTest {

    private UserInterface userInterface;
    @Mock
    ResultSet resultSet;
    @Mock
    ResultSetMetaData metaData;

    @Test
    void printTable() throws SQLException {
        ByteArrayOutputStream outContent = initiateNewOutput();
        userInterface = new UserInterface();
        when(resultSet.getMetaData()).thenReturn(metaData);
        when(resultSet.next()).thenReturn(true, false);
        when(resultSet.getString(1)).thenReturn("data");
        when(metaData.getColumnCount()).thenReturn(1);
        when(metaData.getColumnName(1)).thenReturn("name");
        UserInterface.printTable(resultSet);
        assertEquals("+--------------------------------+" + lineSeparator() +
                "| name                           |" + lineSeparator() +
                "+--------------------------------+" + lineSeparator() +
                "| data                           |" + lineSeparator() +
                "+--------------------------------+" + lineSeparator(), outContent.toString());

    }

    private ByteArrayOutputStream initiateNewOutput() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        return outContent;
    }

    @Test
    void getDatabaseProperties_allCorrect() {
        String ip = "127.0.0.1";
        String port = "8888";
        String user = "user";
        String password = "password";
        setInput(ip + "\r\n" + port + "\r\n" + user + "\r\n" + password);
        DatabaseProperties properties = userInterface.getDatabaseProperties();
        assertEquals(ip + ":" + port, properties.getDatabaseUrl());
        assertEquals(user, properties.getUser());
        assertEquals(password, properties.getPassword());
    }

    @Test
    void getDatabaseProperties_allInSecondTime() {
        String ip1 = "127.0.0";
        String ip2 = "localhost";
        String port1 = "1";
        String port2 = "8888";
        String user1 = "";
        String user2 = "user";
        String password = "";
        setInput(ip1 + "\r\n" + ip2 + "\r\n" + port1 + "\r\n" + port2 + "\r\n" + user1 + "\r\n" + user2 + "\r\n" + password);
        DatabaseProperties properties = userInterface.getDatabaseProperties();
        assertEquals(ip2 + ":" + port2, properties.getDatabaseUrl());
        assertEquals(user2, properties.getUser());
        assertEquals(password, properties.getPassword());
    }


    @Test
    void userWantsToQuit() {
        String input = "q";
        setInput(input);
        assertTrue(userInterface.userWantsToQuit());
    }

    private void setInput(String input) {
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        userInterface = new UserInterface();
    }
}