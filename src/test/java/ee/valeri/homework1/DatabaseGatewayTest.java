package ee.valeri.homework1;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DatabaseGatewayTest {

    private static final int TEST_TIMEOUT = 10;
    @Mock
    MysqlDataSource dataSource;
    @Mock
    Connection connection;
    @Mock
    private DatabaseProperties properties;
    @Mock
    Statement statement;
    private LinkedList<String> queue;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet resultSet;
    @Mock
    private ResultSetMetaData metaData;

    @BeforeEach
    void setUp() {
        queue = new LinkedList<>();
        queue.add("1");
        queue.add("2");
        queue.add("3");
        when(properties.getTimeout()).thenReturn(TEST_TIMEOUT);
    }

    @Test()
    void runInWriteMode_OK() throws SQLException, InterruptedException {
        int size = queue.size();
        mockStatement();
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(1);
        when(statement.executeUpdate(anyString())).thenReturn(1);
        runGateway("write");
        verify(preparedStatement, timeout(0).times(size)).executeUpdate();
        verify(statement, timeout(0).atLeast(2)).executeUpdate(anyString());
        assertTrue(queue.isEmpty());
    }

    private void runGateway(String mode) throws InterruptedException {
        Thread thread = new Thread(new DatabaseGateway(properties, queue, mode));
        thread.start();
        thread.join(50);
    }

    @Test()
    void runInReadMode_OK() throws SQLException, InterruptedException {
        mockStatement();
        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.getMetaData()).thenReturn(metaData);
        when(resultSet.next()).thenReturn(true, false);
        when(resultSet.getString(1)).thenReturn("data");
        when(metaData.getColumnCount()).thenReturn(1);
        when(metaData.getColumnName(1)).thenReturn("name");
        runGateway("read");
        verify(statement, times(1)).executeQuery(anyString());
        verify(metaData).getColumnName(1);
    }

    @Test()
    void runInWriteMode_CreateTableError() throws SQLException, InterruptedException {
        mockStatement();
        when(statement.executeUpdate(anyString())).thenThrow(new SQLException("create database error"));
        runGateway("write");
        verify(statement, timeout(0).atLeast(2)).executeUpdate(anyString());
        assertFalse(queue.isEmpty());
    }

    @Test()
    void runInReadMode_getDataError() throws SQLException, InterruptedException {
        mockStatement();
        when(statement.executeQuery(anyString())).thenThrow(new SQLException("data error"));
        runGateway("read");
        verify(statement, atLeast(2)).executeQuery(anyString());
    }

    private void mockStatement() throws SQLException {
        when(properties.getMysqlDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenReturn(statement);
    }
}