package ee.valeri.homework1;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import static java.lang.System.*;

class UserInterface {
    private static final String PORT_PATTERN = "[0-9]{4,5}";
    private static final String COLUMN_FORMAT = " %-30s |";
    private final Scanner scanner;
    private final DatabaseProperties databaseProperties;
    private static String separatorFormat;

    UserInterface() {
        this.scanner = new Scanner(in);
        this.databaseProperties = new DatabaseProperties();
    }

    static void printTable(ResultSet result) throws SQLException {
        ResultSetMetaData resultSetMetaData = result.getMetaData();
        int columnsNumber = resultSetMetaData.getColumnCount();
        separatorFormat = createSeparator(columnsNumber);
        printHeader(resultSetMetaData, columnsNumber);
        while (result.next()) {
            printRow(result, columnsNumber);
        }
    }

    private static void printHeader(ResultSetMetaData data, int columnsNumber) throws SQLException {
        out.println(separatorFormat);
        out.print("|");
        for (int i = 1; i <= columnsNumber; i++) {
            out.printf(COLUMN_FORMAT, data.getColumnName(i));
        }
        out.println();
        out.println(separatorFormat);
    }

    private static void printRow(ResultSet result, int columnsNumber) throws SQLException {
        out.print("|");
        for (int i = 1; i <= columnsNumber; i++) {
            out.printf(COLUMN_FORMAT, result.getString(i));
        }
        out.println();
        out.println(separatorFormat);
    }

    private static String createSeparator(int columnsNumber) {
        return "+" + new String(new char[columnsNumber])
                .replace("\0", "--------------------------------+");
    }

    static void setInfo(String message) {
        out.println(message);
    }

    DatabaseProperties getDatabaseProperties() {
        askProperties();
        return databaseProperties;
    }

    private void askProperties() {
        databaseProperties.setDatabaseIp(getAddress());
        databaseProperties.setPort(getDatabasePort());
        databaseProperties.setUser(getUser());
        databaseProperties.setPassword(getPassword());
    }

    private String getPassword() {
        setInfoGetPassword();
        scanner.nextLine();
        return scanner.hasNextLine() ? scanner.nextLine() : "";
    }

    private String getUser() {
        String user = "";
        while (user.isEmpty()) {
            setInfoGetUser();
            user = scanner.next();
        }
        return user;
    }

    private String getAddress() {
        String address = "";
        while (!databaseProperties.isAddressValid(address)) {
            setInfoGetAddress();
            address = scanner.next();
        }
        return address;
    }

    private String getDatabasePort() {
        String dbPort = "";
        while (dbPort.isEmpty()) {
            setInfoGetPort();
            dbPort = getPortInput();
        }
        return dbPort;
    }

    private String getPortInput() {
        String port = "";
        try {
            port = scanner.next(PORT_PATTERN);
        } catch (InputMismatchException e) {
            setInfoWrongFormat();
        }
        return port;
    }

    private void setInfoGetPassword() {
        out.println("Please insert database password:");
    }

    private void setInfoGetUser() {
        out.println("Please insert database user:");
    }

    private void setInfoGetAddress() {
        out.println("Please insert database address(e.g. localhost or IP):");
    }

    private void setInfoGetPort() {
        out.println("Please insert database port:");
    }

    private void setInfoWrongFormat() {
        err.println("Wrong format. Please try again");
        scanner.nextLine();
    }

    static void setInfoWriteUnsuccessful(String sqlState, int ms) {
        out.format("%s Waiting %d ms until retry. Insert 'q' to quit %n", sqlState, ms);
    }

    static void setInfoWriteMode() {
        setModeInfo("write");
    }

    static void setInfoReadMode() {
        setModeInfo("read");
    }

    private static void setModeInfo(String mode) {
        out.println("System is launched in data " + mode + " mode, use 'q' for quitting");
    }


    boolean userWantsToQuit() {
        return scanner.next().equals("q");
    }
}