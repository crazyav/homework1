package ee.valeri.homework1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class QueueBuilderTest {

    private LinkedList<String> queue;

    @BeforeEach
    void setUp() {
        queue = new LinkedList<>();

    }

    @Test
    void populatesQueueAfterOneSecond() throws InterruptedException {
        Thread thread = new Thread(new QueueBuilder(queue));
        thread.start();
        thread.join(1000);
        assertFalse(queue.isEmpty());
    }

    @Test
    void populatesQueueAfterTwoSeconds() throws InterruptedException {
        Thread thread = new Thread(new QueueBuilder(queue));
        thread.start();
        thread.join(2000);
        assertEquals(2, queue.size(), 1);
    }
}