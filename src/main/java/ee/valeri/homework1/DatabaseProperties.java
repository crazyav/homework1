package ee.valeri.homework1;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.commons.validator.routines.InetAddressValidator;

class DatabaseProperties {
    static final String COLUMN_NAME_1 = "time_log_id";
    static final String COLUMN_NAME_2 = "time_log_moment";
    static final String DB_NAME = "times";
    static final String TABLE_NAME = "time_log";
    private String ip;
    private String port;
    private String user;
    private String password;


    String getDatabaseUrl() {
        return ip + ":" + port;
    }


    void setDatabaseIp(String ip) {
        this.ip = ip;
    }


    void setPort(String port) {
        this.port = port;
    }

    String getUser() {
        return user;
    }

    void setUser(String user) {
        this.user = user;
    }

    String getPassword() {
        return password;
    }

    void setPassword(String password) {
        this.password = password;
    }

    MysqlDataSource getMysqlDataSource() {
        return new MysqlDataSource();
    }

    boolean isAddressValid(String address) {
        return InetAddressValidator.getInstance().isValid(address) || address.equals("localhost");
    }

    int getTimeout() {
        return 5000;
    }
}
