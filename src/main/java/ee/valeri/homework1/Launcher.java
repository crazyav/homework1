package ee.valeri.homework1;

import java.util.LinkedList;
import java.util.List;

import static java.lang.Thread.currentThread;
import static java.util.Collections.synchronizedList;

public class Launcher {

    private static String mode = "write";
    private static UserInterface userInterface;
    private static List<String> queue;
    private static DatabaseProperties databaseProperties;

    public static void main(String[] args) {
        applyArguments(args);
        setUp();
        startDatabaseGateway();
        if (mode.equals("read")) {
            UserInterface.setInfoReadMode();
        } else {
            UserInterface.setInfoWriteMode();
            startQueueBuilder();
        }
        handleUserQuitRequest();
    }

    private static void setUp() {
        queue = synchronizedList(new LinkedList<>());
        userInterface = new UserInterface();
        databaseProperties = userInterface.getDatabaseProperties();
    }

    private static void handleUserQuitRequest() {
        while (!currentThread().isInterrupted()) {
            sleep();
            if (userInterface.userWantsToQuit()) {
                stop();
            }
        }
    }

    private static void sleep() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void startQueueBuilder() {
        Thread queueBuilder = new Thread(new QueueBuilder(queue));
        queueBuilder.start();
    }

    private static void startDatabaseGateway() {
        Thread database = new Thread(new DatabaseGateway(databaseProperties, queue, mode));
        database.start();
    }

    private static void applyArguments(String[] args) {
        for (String arg : args) {
            if (arg.equals("-p")) {
                mode = "read";
            }
        }
    }

    private static void stop() {
        System.exit(0);
    }
}
