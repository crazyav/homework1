package ee.valeri.homework1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DatabasePropertiesTest {
    private DatabaseProperties properties;

    @BeforeEach
    void setUp() {
        properties = new DatabaseProperties();
    }

    @Test
    void getDatabaseUrl() {
        String ip = "123.1.1.1";
        properties.setDatabaseIp(ip);
        String port = "1234";
        properties.setPort(port);
        assertEquals(ip + ":" + port, properties.getDatabaseUrl());
    }

    @Test
    void isAddressValid_wrongIp() {
        assertFalse(properties.isAddressValid("123644"));
        assertFalse(properties.isAddressValid("wrong"));
    }

    @Test
    void isAddressValid_localhost() {
        assertTrue(properties.isAddressValid("localhost"));
    }

    @Test
    void isAddressValid_rightIp() {
        assertTrue(properties.isAddressValid("127.0.0.1"));
    }
}
