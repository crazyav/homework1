package ee.valeri.homework1;

import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.*;
import java.util.List;

import static ee.valeri.homework1.DatabaseProperties.*;
import static ee.valeri.homework1.UserInterface.printTable;
import static ee.valeri.homework1.UserInterface.setInfoWriteUnsuccessful;
import static java.lang.Thread.currentThread;

class DatabaseGateway implements Runnable {

    private final int timeout;
    private final String mode;
    private final DatabaseProperties properties;
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    private final List<String> queue;
    private final MysqlDataSource dataSource;


    DatabaseGateway(DatabaseProperties properties, List<String> queue, String mode) {
        this.properties = properties;
        this.queue = queue;
        this.mode = mode;
        this.dataSource = properties.getMysqlDataSource();
        this.timeout = properties.getTimeout();
    }

    @Override
    public void run() {
        setUp();
        if (mode.equals("write")) {
            createDatabaseAndTable();
            writeTimes();
        } else {
            readTimes();
        }
        close();
    }

    private void prepareStatement() throws SQLException {
        preparedStatement = connect.prepareStatement("INSERT INTO " + DB_NAME + "." + TABLE_NAME
                + "(" + COLUMN_NAME_2 + ") VALUES(?) ;");

    }

    private void setUp() {
        dataSource.setURL("jdbc:mysql://" + properties.getDatabaseUrl());
        dataSource.setUser(properties.getUser());
        dataSource.setPassword(properties.getPassword());
    }

    private void writeTimes() {
        while (!currentThread().isInterrupted()) {
            if (!queue.isEmpty()) {
                try {
                    connect = dataSource.getConnection();
                    prepareStatement();
                    preparedStatement.setString(1, queue.get(0));
                    int result = preparedStatement.executeUpdate();
                    if (result > 0) {
                        queue.remove(0);
                        close();
                    }
                } catch (SQLException e) {
                    handleExceptionAndSleep(e);
                }
            }
        }
    }

    private void sleep() {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            UserInterface.setInfo(e.getMessage());
            close();
        }
    }

    private void readTimes() {
        while (!currentThread().isInterrupted()) {
            try {
                initializeConnection();
                ResultSet result = statement.executeQuery("SELECT * FROM " + DB_NAME + "." + TABLE_NAME);
                printTable(result);
                result.close();
                break;
            } catch (SQLException e) {
                handleExceptionAndSleep(e);
            }
        }

    }

    private void createDatabaseAndTable() {
        while (!currentThread().isInterrupted()) {
            try {
                initializeConnection();
                createDatabase();
                createTable();
                break;
            } catch (SQLException e) {
                handleExceptionAndSleep(e);
            }
        }

    }

    private void handleExceptionAndSleep(SQLException e) {
        close();
        setInfoWriteUnsuccessful(e.getMessage(), timeout);
        sleep();
    }

    private void initializeConnection() throws SQLException {
        connect = dataSource.getConnection();
        statement = connect.createStatement();
    }

    private void createDatabase() throws SQLException {
        statement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + DB_NAME + ";");
    }

    private void createTable() throws SQLException {
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS " +
                DB_NAME + "." + TABLE_NAME + " " +
                "( " + COLUMN_NAME_1 + " int NOT NULL AUTO_INCREMENT," +
                COLUMN_NAME_2 + " varchar(255) NOT NULL," +
                "PRIMARY KEY (" + COLUMN_NAME_1 + "));");
    }


    private void close() {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connect != null) {
                connect.close();
            }
            System.exit(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}